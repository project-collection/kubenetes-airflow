# kubenetes-airflow

Airflow + Kubenetes + Python


helm chart: https://github.com/airflow-helm/charts/tree/main/charts/airflow
dataset: https://console.cloud.google.com/bigquery?project=bigquery-public-data&ws=!1m5!1m4!4m3!1sbigquery-public-data!2scovid19_aha!3sstaffing&p=bigquery-public-data&d=covid19_aha&t=staffing&page=table


## Setup airflow locally using helm 

tbd


## Setup two DAGs:

####  a 
Copy & if required save the GCP public dataset (link provided) and print the first 20 rows of the table

#### b

Triggered from (a), execute a python script that:

1. Gets / opens this data from (a)
2. Checks this dataset for any county_name which has an average total_personnel_ft <=30
3. Sends a CSV via email with the data row(s) matching (ii) to the recipients listed above

## Create a python file with unit tests for the code executed in 2.b

tbd
